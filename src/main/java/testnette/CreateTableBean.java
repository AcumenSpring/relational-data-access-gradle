package testnette;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class CreateTableBean implements CommandLineRunner
{
	private static final Logger log = LoggerFactory
			.getLogger(CreateTableBean.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public void run(String... strings) throws Exception
	{
		log.info("Creating tables");

		jdbcTemplate.execute("DROP TABLE horse IF EXISTS");

		jdbcTemplate.execute(
				"CREATE TABLE horse(" + "id SERIAL, name VARCHAR(255))");
	}
}
