package testnette;

public class Horse {
    private long id;
    private String name;

    public Horse(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format(
                "Horse[id=%d, name='%s']",
                id, name);
    }

    // getters & setters omitted for brevity
}
